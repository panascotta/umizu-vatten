import sys,argparse,csv
from math import sqrt
from math import sqrt
from math import pi
from math import exp
from random import randrange
from collections import defaultdict

# Split the dataset by class values, returns a dictionary
#separated_by_class
def divideIntoClass(dataset):
    separated = dict()
    for i in range(len(dataset)):
        vector = dataset[i]
        class_value = vector[-1]
        if (class_value not in separated):
            separated[class_value] = list()
        separated[class_value].append(vector)

    return separated


# Load a CSV file
def load_csv(filename):
	with open (filename) as csv_file:
	    dataset = list(csv.reader(csv_file, delimiter=','))
	return dataset


# Convert string column to float
def str_column_to_float(dataset, column):
	for row in dataset:
		row[column] = float(row[column].strip())


# Split a dataset into k folds
def cross_validation_split(dataset, n_folds):
	dataset_split = list()
	dataset_copy = list(dataset)
	fold_size = int(len(dataset) / n_folds)
	for _ in range(n_folds):
		fold = list()
		while len(fold) < fold_size:
			index = randrange(len(dataset_copy))
			fold.append(dataset_copy.pop(index))
		dataset_split.append(fold)
	return dataset_split


# Calculate accuracy percentage
def accuracy_metric(actual, predicted):
	correct = 0
	for i in range(len(actual)):
		if actual[i] == predicted[i]:
			correct += 1
	return correct / float(len(actual)) * 100.0


# Evaluate an algorithm using a cross validation split
def evaluate_algorithm(dataset, algorithm, n_folds, *args):
	folds = cross_validation_split(dataset, n_folds)
	scores = list()
	for fold in folds:
		train_set = list(folds)
		train_set.remove(fold)
		train_set = sum(train_set, [])
		test_set = list()
		for row in fold:
			row_copy = list(row)
			test_set.append(row_copy)
			row_copy[-1] = None
		predicted = algorithm(train_set, test_set, *args)
		actual = [row[-1] for row in fold]
		accuracy = accuracy_metric(actual, predicted)
		scores.append(accuracy)
	return scores

# Calculate the counts for each value for each column in a dataset.
# summarize_by_class call summarize_dataset for each class
# Hence summarize_dataset returns the information of that class,
# which is a list of tuple with length = the number of columns
# Each tuple consists of the counts for each value for that column
def summarize_dataset(dataset):
    summaries = []

    for column in list(zip(*dataset))[:-1]:
		# A dictionary (key: a value in that column, value: how many that value appears in that column)
		# A dictionary refers to a column
		# example: count = {0.25: 5, 0.5: 1, 0.75: 3, 1: 5}
		# It means in that column (remember: a dictionary represents a column) there are 5 rows
		# whose values are 0.25, 1 row whose value is 0.5, 3 rows whose values are 0.75, 
		# and 5 rows whose values are 1
        counts = defaultdict(lambda:0, {})

        for element in column:

            element = float(element)

            if element not in counts:
                counts[element] = 1
            else:
                counts[element] += 1

        summaries.append(counts)

	# Return a list that contains 3 dictionaries(column)
    return summaries

# Split dataset by class then calculate statistics for each row
# return a dict with class value as a key and the statistics for that class
def summarize_by_class(dataset):
  separated = divideIntoClass(dataset)

  result = dict()
  
  #clss --> key in separated; rows -->clss' value in separated
  for clss, rows in separated.items():
    # A key will store a list that contains 3 dictionary(column)
	# Each dictionary stores the frequency of the data in that column
	# First dictionary: air_penting
	# Second dictionary: air_dekat
	# Third dictionary: camilan
    result[clss] = summarize_dataset(rows)

  # example: result={0.25: [{0.25: 5, 0.5: 1, 0.75: 3, 1: 5}, {...}, {...}], 0.5: [{...}, {...}, {...}],...}
  # 0.25: [{0.25: 5, ... --> it means that in class 0.25 (very inconsistent) column air penting (since it's first dict)
  # the occurence of 0.25 whose class is 0.25(very inconsistent) in column air penting are 5
  return result
 
# Calculate the probabilities of predicting each class for a given row
def calculateProbabilities(summaries, row):
	total_rows = sum([sum(summaries[label][0].values()) for label in summaries])
	probabilities = dict()

	print(row)
	for class_value, class_summaries in summaries.items():
    	# summaries[class_value][0]
		# for example: class value is 0.25 (very inconsistent), then
		# summaries[class_value][0] is a dictionary that represents column air penting in class very inconsistent
		# summaries[class_value][0] = {0.25: 5, 0.5: 1, 0.75: 3, 1: 5}
		# summaries[class_value][0].values() = [5, 1, 3, 5]
		# sum(summaries[class_value][0].values()) = sum([5, 1, 3, 5]) = 14
		# so sum(summaries[class_value][0].values()) is how many rows that belongs to class very inconsistent
		# sum(summaries[class_value][0].values()) / float(total_rows) --> p(class)
		# p(class) = how many rows belong to that class / total row
		probabilities[class_value] = sum(summaries[class_value][0].values()) / float(total_rows)
		# untuk setiap kolom selain label

		# untuk setiap kolom selain label class (kolom air penting, air dekat, camilan)
		for i in range(len(class_summaries)):
			# banyaknya yang kelas class_value dan kolom i-nya sama / banyaknya yang kelas class_value
			# class_summaries: list yang isinya 3 dictionary
			# row: list yang isinya input pengguna
			# class_summaries[i][row[i]]: banyaknya data pada kolom ke-i dengan key input pengguna ke-i
			# misalnya: class_summaries = [{0.25: 5, 0.5: 1, 0.75: 3, 1: 5}, {...}, {...}]
			# misalnya i saat ini 0
			# class_summaries[0] = {0.25: 5, 0.5: 1, 0.75: 3, 1: 5}
			# misalnya row = [0.25, 0.5, 0.75, 0.5]
			# row[0] = 0.25
			# class_summaries[i][row[i]] --> class_summaries[0][0.25] = 5
			# class_summaries[0][0.25] --> banyaknya kemunculan nilai 0.25 pada kolom air penting
			probabilities[class_value] *= class_summaries[i][row[i]] / sum(class_summaries[i].values())
            
	return probabilities

# Predict the class for a given row
def predict(summaries, row):
	#row = input pengguna
	probabilities = calculateProbabilities(summaries, row)
	best_label, best_prob = None, -1
	for class_value, probability in probabilities.items():
		if best_label is None or probability >= best_prob:
			best_prob = probability
			best_label = class_value
	best_prob *=100

	return best_label

# Naive Bayes Algorithm
def naive_bayes_algo(train, test):
	summarize = summarize_by_class(train)
	predictions = list()
	for row in test:
		output = predict(summarize, row)
		predictions.append(output)
	return(predictions)

# Test Naive Bayes on Iris Dataset
filename = 'use.csv'
dataset = load_csv(filename)
for i in range(len(dataset[0])):
	str_column_to_float(dataset, i)

# evaluate algorithm
n_folds = 5
scores = evaluate_algorithm(dataset, naive_bayes_algo, n_folds)
print('Scores: %s' % scores)
print('Mean Accuracy: %.3f%%' % (sum(scores) / float(len(scores))))