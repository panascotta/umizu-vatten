from django.shortcuts import render
from .forms import UserForm
from .ai import *

# Create your views here.
def main(request):
    return render(request, 'main.html')

def res(request):
    category = 0.75
    template = "category_{}.html".format(category)
    return render(request, template)

def userForm(request):
    if request.method == "POST":
        # get the field from form
        air_penting = float(request.POST.get('air_penting'))
        air_dekat = float(request.POST.get('air_dekat'))
        camilan = float(request.POST.get('camilan'))

        # Store the input in list
        input_pengguna = list()
        input_pengguna.append(air_penting)
        input_pengguna.append(air_dekat)
        input_pengguna.append(camilan)
        
        # Call the ai
        filename = 'use.csv'
        dataset = load_csv(filename)
        summaries = summarize_by_class(dataset)
        hasil = predict(summaries, input_pengguna)

        template = "category_{}.html".format(hasil)
        args = {
            'hasil': hasil,
        }
        return render(request, template , args)
    
    form = UserForm()
    args = {
        'form': form
    }
    return render(request, 'form.html', args)
