from django.urls import path
from . import views

app_name = "main"

urlpatterns = [
    path('', views.main, name='main'),
    path('result', views.res, name = 'results'),
    path('form/', views.userForm, name='userForm')

]