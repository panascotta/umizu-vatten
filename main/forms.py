from django import forms

jenis_kelamin_choices =[
    ('Laki-laki', 'Male'),
    ('Perempuan', 'Female')
]

air_penting_choices = [
    (0.25,1),
    (0.5,2),
    (0.75,3),
    (1,4)
]

air_dekat = [
    (1, "Yes"),
    (0,"No"),
]

camilan_choices = [
    (0.25, "Maximum once a week"),
    (0.5, "2-4 times a week"),
    (0.75, "5-6 times a week"),
    (1, "Every day")
]

air_choices = [
    (0.25, "0-1 glass per day"),
    (0.5, "2-4 glasses per day"),
    (0.75, "5-7 glasses per day"),
    (1, ">= 8 glasses per day")
]



class UserForm(forms.Form):
    nama = forms.CharField(
        max_length=200,
        widget=forms.TextInput(
            attrs={
                'class': 'text-input',
                'placeholder': 'type your name',
            }
        )
    )

    umur = forms.IntegerField(
        widget=forms.NumberInput(
             attrs={
                'class': 'text-input',
                'placeholder': 'type your age',
            }
        )
    )

    jenis_kelamin = forms.ChoiceField(
        widget=forms.RadioSelect(
           attrs={
               'class': 'rad-but',
           }
        ),
        choices=jenis_kelamin_choices,
    )

    air_penting = forms.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
               'class': 'rad-but',
               'id': 'inl',
           }
        ),
        choices=air_penting_choices,
    )

    air_dekat = forms.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
               'class': 'rad-but',
           }
        ),
        choices=air_dekat,
    )

    camilan = forms.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
               'class': 'rad-but',
           }
        ),
        choices=camilan_choices,
    )

    air = forms.ChoiceField(
        widget=forms.RadioSelect(
            attrs={
               'class': 'rad-but',
           }
        ),
        choices= air_choices,
    )
